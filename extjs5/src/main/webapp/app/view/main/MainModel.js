/* 
 * 视图模型 - 系统主页
 */

Ext.define("App.view.main.MainModel", {
	extend: "Ext.app.ViewModel",
	alias: "viewmodel.main",
	
	data: {name:'学生管理系统'},
	stores: []
});