Ext.define("websystem.view.student.Student", {
	extend : 'Ext.grid.Panel',
	/*
	 * requires:[ "websystem.view.student.StudentController",
	 * "websystem.view.student.StudentModel" ],
	 */
	xtype : "student",
	id : "student",
	closable : true,
	closeAction : "hide",
	initComponent : function() {

		var ds = Ext.create("websystem.store.StudentStore");
		ds.on("beforeload", function() {
			var name =Ext.getCmp("name");
    		var role =Ext.getCmp("role");
    		var state =Ext.getCmp("state");
    	if(typeof(name)!="undefined"&&typeof(role)!="undefined"&&typeof(state)!="undefined"){
    		ds.getProxy().setExtraParams({
    			"name":name.getValue()
    		});
    	}
    	
			
		});
	
		ds.load({
					params : {
						limit : 2,
						page : 0,
						start : 0

					}
				}), Ext.apply(this, {

					store : ds,
					selType : "checkboxmodel",
					title : "学生信息",
					border : true,
					columns : [{
								text : "学生编号",
								dataIndex : "id",
								hidden : true
							}, {
								text : "学生姓名",
								dataIndex : "name",
								flex : 1
							}, {
								text : "学生职位",
								dataIndex : "post",
								flex : 1,
								renderer : function(val) {
									if (val == "1") {
										return "学生会主席";
									} else if (val == "2") {
										return "三好学生";
									} else if (val == "3") {
										return "普通学生";
									}
									return val;
								}
							}, {
								text : "状态",
								dataIndex : "state",
								flex : 1,
								renderer : function(val) {
									if (val == "5" || val == "启用") {
										return "<span style='color:green;'>启用</span>";
									} else if (val == "4" || val == "禁用") {
										return "<span style='color:red;'>禁用</span>";
									}
									return val;
								}
							}, {
								text : "备注",
								dataIndex : "remark",
								flex : 2
							}, {
								xtype : "actioncolumn",
								text : "操作",
								width : 100,
								align : "center",
								sortable : false,
								menuDisabled : true,
								items : [{
											iconCls : "opt-edit",
											tooltip : "编辑",
											handler : "edit"
										}, {
											iconCls : "opt-delete",
											tooltip : "删除",
											handler : "del"
										}]
							}],
					tbar : [{
								xtype : "textfield",
								maxWidth : 205,
								fieldLabel : "用户名称",
								id : "name",
								labelWidth : 60
							}, {
								xtype : "combobox",
								maxWidth : 205,
								fieldLabel : "用户角色",
								id : "role",
								labelWidth : 60,
								displayField : "dict_value",
								valueField : "dict_id",
								store : Ext.create("websystem.store.ComBoBox")
							}, {
								xtype : "combobox",
								maxWidth : 180,
								fieldLabel : "状态",
								labelWidth : 35,
								id : "state",
								displayField : "dict_value",
								valueField : "dict_id",
								queryMode : "local",
								store : Ext.create("websystem.store.States")
							}, {
								xtype : "button",
								text : "搜索",
								glyph : 0xf002,
								handler : "clickSearchCondition"

							}, "->", "->", {
								xtype : "button",
								text : "新增",
								glyph : 0xf067,
								handler : "add"
							}, {
								xtype : "button",
								text : "批量删除",
								glyph : 0xf00d,
								handler : "batchDel"
							}],
					bbar : {
						emptyMsg : "没有数据",
						xtype : "pagingtoolbar",
						displayInfo : true,
						store : ds,
						beforePageText : "当前是第",
						afterPageText : "页,一共{0}页",
						displayMsg : "显示从{0}条数据到{1}条数据,共{2}条数据"
					},
					listeners : {
						itemclick : function(view, record, item, index, e,
								eOpts) {
						}
					}
				})
		this.callParent(arguments);
	}

})