Ext.define("websystem.view.main.West", {
			extend : "Ext.tree.Panel",
			xtype : "navigation",
			id : "navigation",
			initComponent : function() {
				var store = Ext.create('Ext.data.TreeStore', {
							root : {
								text : "系统菜单",
								id : "root",
								expanded : true,
								children : [{
											text : "用户管理",
											id : "um",
											children : [{
														text : "查看学生信息",
														id:"find",
														leaf : true
													}, {
														text : "修改学生信息",
														leaf : true
													}, {
														text : "添加学生信息",
														leaf : true
													}, {
														text : "添加学生信息",
														leaf : true
													}]
										}, {
											text : "成绩管理",
											children : [{
														text : "查看成绩信息",
														leaf : true
													}, {
														text : "修改成绩信息",
														leaf : true
													}, {
														text : "添加成绩信息",
														leaf : true
													}, {
														text : "添加成绩信息",
														leaf : true
													}]
										}, {
											text : "课程管理",
											children : [{
														text : "查看课程信息",
														leaf : true
													}, {
														text : "修改课程信息",
														leaf : true
													}, {
														text : "增加课程信息",
														leaf : true
													}, {
														text : "删除课程信息",
														leaf : true
													}]
										}, {
											text : "报名管理",
											children : [{
														text : "查看报名信息",
														leaf : true
													}, {
														text : "修改报名信息",
														leaf : true
													}, {
														text : "增加报名信息",
														leaf : true
													}, {
														text : "删除报名信息",
														leaf : true
													}]
										}]
							}
						});

				Ext.apply(this, {
							rootVisible : false,
							store : store,
							listeners:{itemclick:"onClickButton"}
						});
				this.callParent(arguments);
			}
		})