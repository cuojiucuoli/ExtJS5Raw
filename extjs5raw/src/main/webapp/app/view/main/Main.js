/**
 * This class is the main view for the application. It is specified in app.js as
 * the "autoCreateViewport" property. That setting automatically applies the
 * "viewport" plugin to promote that instance of this class to the body element.
 * 
 * TODO - Replace this content of this view to suite the needs of your
 * application.
 */
Ext.define('websystem.view.main.Main', {
			extend : 'Ext.container.Container',
			requires : ['websystem.view.main.MainController',
					'websystem.view.main.MainModel','websystem.store.StudentStore',"websystem.store.ComBoBox","websystem.store.States"],
			xtype : 'main',

			controller : 'main',

			uses : ["websystem.view.main.West",
					'websystem.view.student.Student'
					],

			viewModel : {
				type : 'main'
			},

			layout : {
				type : 'border'
			},

			items : [{
						xtype : 'navigation',
						bind : {
							title : '{name}'
						},
						region : 'west',
						collapsible : true,
						width : 250,
						split : true

					}, {
						region : 'center',
						xtype : 'tabpanel',
						id : "center",
						items : [{
									bind : {
										title : '{name}'
									}

								}]
					}, {
						region : 'north',
						xtype : 'tabpanel'
					}, {
						region : 'south',
						xtype : 'tabpanel'
					}]
		});
