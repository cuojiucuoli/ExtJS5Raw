/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('websystem.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        name: '学生管理系统'
    }

    //TODO - add data, formulas and/or methods to support your view
});