/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('websystem.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.window.MessageBox'     
    ],
   	
    alias: 'controller.main',

    onClickButton: function (self,record,item,index,e,eo) {
       if(record.get("text")=="查看学生信息"){
       	var center = Ext.getCmp("center");
       	var  tab =center.getActiveTab();
       	if(tab.id!="tab1"){
       	center.add({
       		xtype:"tabpanel",
       		id:"tab1",
       		title:"查看学生信息",
       		closable:true,
       		closeAction:"hide",
       		items:[{
       		xtype:"student"
       		}]
       		
       	});
       		center.setActiveItem(Ext.getCmp("tab1"));
       	}
       
       }
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },
    
      clickSearchCondition: function (self,record,item,index,e,eo) {	
    	var name =Ext.getCmp("name").getValue();
    	var role =Ext.getCmp("role").getValue();
    	var state =Ext.getCmp("state").getValue();
    	
 		var store = Ext.StoreMgr.get('student');

 		store.load({
 			params:{
 				"name":name,
 				"state":state,
 				"post":role
 				
 			}
 		});
 	
 		
   	}
    
});
