Ext.define("websystem.store.StudentStore",{
	extend:"Ext.data.Store",
	pageSize:2,
	alias:"store.student",
		storeId:"student",
	xtype:"mystore",
	model:"websystem.model.StudentModel",
	proxy:{
		type:"ajax",
		url:"findBySpecification.do",
		reader:{
			type:"json",
			rootProperty:"data",
			totalProperty:"total"
		}
	}

})