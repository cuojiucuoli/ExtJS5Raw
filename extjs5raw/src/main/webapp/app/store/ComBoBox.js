Ext.define("websystem.store.ComBoBox",{
	extend:"Ext.data.Store",
	alias:"store.ComBoBox",
	xtype:"ComBoBoxStore",
	fields:['dict_id','dict_value'],
	proxy:{
		type:"ajax",
		url:"getComBoBox.do",
		reader:{
			type:"json"
		}
	},
	autoLoad:true
})