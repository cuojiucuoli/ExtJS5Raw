package com.theanswr.view;

import java.awt.print.Pageable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.SynthesizedAnnotation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.theanswr.bean.ComBox;
import com.theanswr.bean.Student;
import com.theanswr.dao.StudentDao;

@Controller
public class StudentView {

	@Autowired
	StudentDao dao;

	@RequestMapping(value = "/getComBoBox.do", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getComBoBox() {
		List<ComBox> roleByDict = dao.getRoleByDict();
		String jsonString = JSON.toJSONString(roleByDict);
		return jsonString;
	}

	@RequestMapping(value = "/getComBoBox2.do", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getComBoBox2() {
		List<ComBox> statesByDict = dao.getStatesByDict();
		String jsonString = JSON.toJSONString(statesByDict);

		return jsonString;
	}

	@RequestMapping(value = "/findBySpecification.do", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String findBySpecification(Student student, Integer start, Integer limit) {
		Integer page=start/limit;
		System.out.println(page + limit);
		Specification<Student> spec = new Specification<Student>() {

			@Override
			public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				if (!StringUtils.isEmpty(student.getName())) {
					String name = student.getName();
					String name1 = null;
					try {
						name1 = new String(name.getBytes("ISO-8859-1"), "utf-8");

					} catch (Exception e) {
						e.printStackTrace();
					}
					predicates.add(cb.like(root.get("name").as(String.class), "%" + name1.trim() + "%"));
				}
				if (!StringUtils.isEmpty(student.getPost())) {
					predicates.add(cb.equal(root.get("post").as(String.class), student.getPost()));
				}
				if (!StringUtils.isEmpty(student.getState())) {
					predicates.add(cb.equal(root.get("state").as(String.class), student.getState()));
				}
				if (predicates.size() == 0) {
					return null;
				}
				Predicate[] pre = new Predicate[predicates.size()];
				Predicate or = cb.and(predicates.toArray(pre));
				return or;
			}
		};
		
		PageRequest pageRequest = new PageRequest(page, limit);
		Page<Student> pages = dao.findAll(spec, pageRequest);
		List<Student> content = pages.getContent();
		
		long totalElements = pages.getTotalElements();
		Map map = new HashMap();
		map.put("data", content);
		map.put("total", totalElements);
		String jsonString = JSON.toJSONString(map);
		System.out.println(jsonString);
		return jsonString;
	}
}
