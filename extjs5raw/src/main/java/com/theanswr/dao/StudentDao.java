package com.theanswr.dao;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.theanswr.bean.ComBox;
import com.theanswr.bean.Student;

public interface StudentDao extends JpaRepository<Student, Integer>,JpaSpecificationExecutor<Student>,PagingAndSortingRepository<Student,Integer>{
	@Query("from ComBox where dict_code = 001")
	List<ComBox> getRoleByDict();
	
	@Query("from ComBox where dict_code=002")
	List<ComBox> getStatesByDict();
	
}
