package com.theanswr.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.theanswr.bean.Student;
import com.theanswr.dao.StudentDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/applicationContext.xml")
public class MyTest {

	@Autowired
	StudentDao dao;
	
	@Test
	public void searchMyStudent() {
		
		List<Student> findAll = dao.findAll();
		System.out.println(findAll);
	}
}
